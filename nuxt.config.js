module.exports = {
  head: {
    title: "learnCAPITALS",
    meta: [{
        charset: 'utf-8'
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },
      {
        hid: 'description',
        name: 'description',
        content: 'learnCAPITALS'
      }
    ],
    link: [{
      rel: 'stylesheet',
      href: 'https://fonts.googleapis.com/css?family=Atma'
    }]
  },
  css: [
    '@/assets/main.css'
  ],
}