import Vuex from 'vuex'


const store = () => {
  return new Vuex.Store({
    state: () => ({
      win: false,
      guess: 0
    }),

    mutations: {
      guessed(state) {
        state.guess++
      },
      correct(state) {
        state.win = true
      },
      wrong(state) {
        state.win = false
      },
      resetGuess(state) {
        state.guess = 0
      }
    },

    actions: {
      guessed: ({
        commit
      }) => commit('guessed'),
      correct: ({
        commit
      }) => commit('correct'),
      wrong: ({
        commit
      }) => commit('wrong'),
      resetGuess: ({
        commit
      }) => commit('resetGuess')
    }
  })
}

export default store